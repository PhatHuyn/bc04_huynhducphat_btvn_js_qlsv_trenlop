function laythongtintuForm(){
    const maSV = document.getElementById('txtMaSV').value;
    const TenSV = document.getElementById('txtTenSV').value;
    const EmailSV = document.getElementById('txtEmail').value;
    const PassSV = document.getElementById('txtPass').value;
    const DiemToan = document.getElementById('txtDiemToan').value;
    const DiemLy = document.getElementById('txtDiemLy').value;
    const DiemHoa = document.getElementById('txtDiemHoa').value;

    return new SinhVien(maSV, TenSV, EmailSV, PassSV, DiemToan, DiemLy, DiemHoa);
}

// render array sv ra giao dien.
function renderDSSV(svArr){

    var contentHTML = "";
    for(var i = 0; i < svArr.length; i++){
        var sv = svArr[i];
        //trcontent trong mỗi lần lập
        var trContent = `<tr id="${sv.maSV}">
        <td>${sv.maSV}</td>
        <td>${sv.tenSV}</td>
        <td>${sv.emailSV}</td>
        <td>${sv.tinhDTB()}</td>
        <td>
        <button onclick="xoaSinhVien('${sv.maSV}')" class="btn btn-danger">Xoá</button>
        <button onclick="suaSinhVien('${sv.maSV}')" class="btn btn-warning">Sửa</button>
        </td>
        </tr>`;
        contentHTML += trContent;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;    
}

function timKiemViTri(id, dssv){
    for(var index = 0; index<dssv.length; index++){
        var sv = dssv[index];
        if(sv.maSV == id){
            return index;
        }
    }
    return -1;
}

// function TimTenSV(name, dssv){
//     for(var index = 0;index<dssv.length;index++){
//         var sv = dssv[index];
//         console.log('sv.tenSV: ', sv.tenSV);
//         if(sv.tenSV == name){
//             return index;
//         }
//     }
//     return -1;
// }

function TimTenSV(name, dssv){
    for(var index = 0;index<dssv.length;index++){        
        var sv = dssv[index];
        if(sv.tenSV.search(name) != -1){
            return sv;
        }
    }
    return -1;
}

function showThongTinLenForm(sv){
    document.getElementById('txtMaSV').value = sv.maSV ;
    document.getElementById('txtTenSV').value = sv.tenSV;
    document.getElementById('txtEmail').value = sv.emailSV;
    document.getElementById('txtPass').value = sv.matKhau;
    document.getElementById('txtDiemToan').value = sv.Diemtoan;
    document.getElementById('txtDiemLy').value = sv.Diemly;
    document.getElementById('txtDiemHoa').value = sv.Diemhoa;
}

function CapNhatSV(svCapNhat, dssv){
    // svCapNhat.maSV = document.getElementById('txtMaSV').value;
    console.log('svCapNhat.maSV: ', svCapNhat.maSV);

    for(var i = 0; i<dssv.length;i++){
        var svUpdate = dssv[i];
        console.log('svUpdate: ', svUpdate);
        if(svCapNhat.maSV == svUpdate.maSV){
            svUpdate.tenSV = svCapNhat.tenSV;
            svUpdate.emailSV = svCapNhat.emailSV;
            svUpdate.matKhau = svCapNhat.matKhau;
            svUpdate.Diemtoan = svCapNhat.Diemtoan;
            svUpdate.Diemly = svCapNhat.Diemly;
            svUpdate.Diemhoa = svCapNhat.Diemhoa;
        }
    }
    
    console.log('dssv: ', dssv);
    return dssv;
}

