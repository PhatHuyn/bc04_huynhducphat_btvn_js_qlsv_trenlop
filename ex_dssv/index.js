const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

//Chức năng thêm sv
var dssv = [];


// lấy thông tin từ localStorage
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if(dssvJson != null){
    dssv = JSON.parse(dssvJson);

    for(var index = 0; index < dssv.length; index++){
        var sv = dssv[index];
        dssv[index] = new SinhVien(
            sv.maSV, 
            sv.tenSV, 
            sv.emailSV, 
            sv.matKhau, 
            sv.Diemtoan, 
            sv.Diemly, 
            sv.Diemhoa
        );
    }

    renderDSSV(dssv);
}


function themSV(){
    var newSV = laythongtintuForm();
    // kiểm tra
    var isValid = validator.kiemTraRong(newSV.maSV, "spanMaSV", "Mã sinh viên không được để rỗng.") &&
    validator.kiemTraDoDai(newSV.maSV, "spanMaSV", "Mã sinh viên phải gồm 4 ký tự", 4, 4);
    //
    isValid = isValid &
    // console.log('isValid2: ', isValid);
    validator.kiemTraRong(newSV.tenSV, "spanTenSV", "Tên sinh viên không được để rỗng");
    //
    isValid = isValid &
    validator.kiemTraRong(newSV.emailSV, "spanEmailSV", "Email không được để rỗng") &&
    validator.kiemtraEmail(newSV.emailSV, "spanEmailSV", "Email sinh viên không hợp lệ");
    console.log('isValidEmail: ', isValid);

    isValid = isValid &
    validator.kiemTraRong(newSV.matKhau, "spanMatKhau","Mật khẩu không được để rỗng") &
    validator.kiemTraRong(newSV.Diemtoan, "spanToan", "Điểm toán không được để rỗng") &
    validator.kiemTraRong(newSV.Diemly, "spanLy", "Điểm lý không được để rỗng") & 
    validator.kiemTraRong(newSV.Diemhoa, "spanHoa", "Điểm hoá không được để rỗng");
    
    if(isValid){
    dssv.push(newSV);
    //tạo json
    var dssvJson = JSON.stringify(dssv);
    //lưu json vào localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
    }
}

function xoaSinhVien(id){
    console.log(id);

    // var index = dssv.findIndex( function(sv){
    //     return sv.maSV == id;
    // });

    var index = timKiemViTri(id, dssv);
    if(index != -1){
    dssv.splice(index, 1);
    //tạo json
    var dssvJson = JSON.stringify(dssv);
    //lưu json vào localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
    }
}

function suaSinhVien(id){
    var index = timKiemViTri(id, dssv);
    console.log('index: ', index);
    
    if(index != -1){
        var sv = dssv[index];
        showThongTinLenForm(sv);
    }
}

var dssvnew = [];

function TimKiemSV(){
    var name = document.getElementById("txtSearch").value;
    var index = TimTenSV(name, dssv);

    if(index != -1){    
        console.log('index2 : ', index);
        dssvnew.push(index);
        renderDSSV(dssvnew);
    }
}

function CapNhat(){
    var UpdateSV = laythongtintuForm();
    //kiểm tra
    var isValid = validator.kiemTraRong(UpdateSV.tenSV, "spanTenSV", "Tên sinh viên không được để rỗng");

    isValid = isValid &
    validator.kiemTraRong(UpdateSV.emailSV, "spanEmailSV", "Email không được để rỗng") &&
    validator.kiemtraEmail(UpdateSV.emailSV, "spanEmailSV", "Email sinh viên không hợp lệ");
    console.log('isValidEmail: ', isValid);

    isValid = isValid &
    validator.kiemTraRong(UpdateSV.matKhau, "spanMatKhau","Mật khẩu không được để rỗng");
    console.log('isValidmk: ', isValid);
    isValid = isValid &
    validator.kiemTraRong(UpdateSV.Diemtoan, "spanToan", "Điểm toán không được để rỗng");
    console.log('isValidToan: ', isValid);
    isValid = isValid &
    validator.kiemTraRong(UpdateSV.Diemly, "spanLy", "Điểm lý không được để rỗng");
    console.log('isValidLy: ', isValid);
    isValid = isValid &
    validator.kiemTraRong(UpdateSV.Diemhoa, "spanHoa", "Điểm hoá không được để rỗng");
    console.log('isValidHoa: ', isValid);

    if(isValid){
    CapNhatSV(UpdateSV, dssv);
    //tạo json
    var dssvJson = JSON.stringify(dssv);
    //lưu json vào localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
    }
}